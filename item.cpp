
/*
 *
 *Author: Quentin Levent
 *
 */
#include <iostream>
#include "item.hpp"
#include <SFML/System/String.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <math.h>
#include "objets.hpp"
#include "entites.hpp"


//Fonctions pour l'arme---------------------- {{{
void Arme::use(Characters& perso)
{
	perso.PrendreDegat(this->nombreDegat);
}
//}}}
