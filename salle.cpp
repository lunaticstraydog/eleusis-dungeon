/*
 *
 *Author: Quentin Levent
 *
 */
#include <SFML/Window/Window.hpp>
#include <SFML/System/String.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/String.hpp>
#include <iostream>
#include <math.h>
#include "salle.hpp"
#include "labyrinthe.hpp"
#include "item.hpp"
#include "entites.hpp"
//Render - Affiche la salle et tous ses éléments constitutifs---------------------- {{{
void Salle::render(sf::RenderTarget& target)
{
	target.draw(this->sprite);
	for (auto & enemy : this->enemies)
	{
		enemy.render(target);
	}
	for (auto & item : this->items)
	{
		item.render(target);
	}
	std::vector<std::vector<Border> >::iterator row;  //mieux itérateur!
	std::vector<Border>::iterator col;
	for (row=borders.begin();row!=borders.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			col->render(target);
		}
	}
}
//}}}
//tour suivant - Fait passer un tour a  tous les elements de la salle---------------------- {{{
void Salle::tourSuivant(Player& player)
{
	for (auto & enemy : this->enemies)
	{
		enemy.AutoAttack(player);
	}
}
//}}}
//update Fait bouger tous les elements de la salle---------------------- {{{
bool Salle::update(sf::Window& fenetre,Labyrinthe& laby)
{
	readyForNextTurn=true;
	for (auto & enemy : this->enemies)
	{
		if (!enemy.update(fenetre,laby))
		{
			this->readyForNextTurn=false; //il y a au moins un ennemi qui est pas encore prêt
		}
	}
	return readyForNextTurn;
}
//}}}
//Fonction de création de bordures autour de la salle---------------------- {{{
void Salle::createBorders(std::vector<int>& adjacentRooms)    //left - right -up - down
{
	Border * borderette;
	borders.push_back(std::vector<Border>()); //on crée les 4 lignes du vecteur
	borders.push_back(std::vector<Border>());
	borders.push_back(std::vector<Border>());
	borders.push_back(std::vector<Border>());
	//si salle a gauche -> 2 bordures a gauche---------------------- {{{
	if (adjacentRooms.at(0))
	{
		//std::cout << "Il y a une salle a gauche" << std::endl;
		/*
			 borderette=new Border(this->textureBordure,0,0,1,this->largeurBordure, this->getSizey()/2-tailleDemiPorte,this->getSizex(),this->getPosition().y); //premiere moitié de la bordure gauche
			 borders.at(0).push_back(*borderette);
			 borderette=new Border(this->textureBordure,0,0,1,this->largeurBordure, this->getSizey()/2-tailleDemiPorte,this->getSizex(),this->getPosition().y);
			 borders.at(0).push_back(*borderette);
			 */
	}
	else
	{
		borderette=new Border(this->textureBordure,0,0,1,this->largeurBordure, this->getSizey(),this->getPosition().x,this->getPosition().y); //bordure gauche
		borders.at(0).push_back(*borderette);
	}
	//}}}
	//si salle a droite -> 2 bordures a droite---------------------- {{{
	if (adjacentRooms.at(1))
	{
		//std::cout << "Il y a une salle a droite" << std::endl;
		borderette=new Border(this->textureBordure,0,0,1,this->largeurBordure, this->getSizey()/2-tailleDemiPorte,this->getPosition().x+this->getSizex()-this->largeurBordure,this->getPosition().y); //premiere moitié de la bordure droite
		borders.at(1).push_back(*borderette);
		borderette=new Border(this->textureBordure,0,0,1,this->largeurBordure, this->getSizey()/2-tailleDemiPorte,this->getPosition().x+this->getSizex()-this->largeurBordure,this->getPosition().y+this->getSizey()/2+tailleDemiPorte);
		borders.at(1).push_back(*borderette);
	}
	else
	{
		borderette=new Border(this->textureBordure,0,0,1,this->largeurBordure, this->getSizey(),this->getSizex()+this->getPosition().x-this->largeurBordure,this->getPosition().y); //bordure droite
		borders.at(1).push_back(*borderette);
	}
	//}}}
	//si salle en haut -> 2 bordures en haut---------------------- {{{
	if (adjacentRooms.at(2))
	{
		//std::cout << "Il y a une salle en haut" << std::endl;
		/*
			 borderette=new Border(this->textureBordure,0,0,1,this->getSizex()/2-tailleDemiPorte,this->largeurBordure ,this->getPosition().x,this->getPosition().y); //premiere moitié de la bordure haute
			 borders.at(0).push_back(*borderette);
			 borderette=new Border(this->textureBordure,0,0,1,this->getSizex()/2-tailleDemiPorte,this->largeurBordure ,this->getPosition().x+this->getSizex()+2*tailleDemiPorte,this->getPosition().y); //deuxième moitié de la bordure haute
			 borders.at(0).push_back(*borderette);
			 */
	}
	else
	{
		borderette=new Border(this->textureBordure,0,0,1,this->getSizex(),this->largeurBordure ,this->getPosition().x,this->getPosition().y); //grande bordure qui fait toute la largeur de la salle
		borders.at(2).push_back(*borderette);
	}
	//}}}
	//si salle en bas -> 2 bordures en bas---------------------- {{{
	if (adjacentRooms.at(3))
	{
		borderette=new Border(this->textureBordure,0,0,1,this->getSizex()/2-tailleDemiPorte,this->largeurBordure ,this->getPosition().x,this->getPosition().y+this->getSizey()-this->largeurBordure); //premiere moitié de la bordure basse
		borders.at(3).push_back(*borderette);
		borderette=new Border(this->textureBordure,0,0,1,this->getSizex()/2-tailleDemiPorte,this->largeurBordure ,this->getPosition().x+this->getSizex()/2+tailleDemiPorte,this->getPosition().y+this->getSizey()-this->largeurBordure); //deuxième moitié de la bordure basse
		borders.at(3).push_back(*borderette);
	}
	else
	{
		borderette=new Border(this->textureBordure,0,0,1,this->getSizex(),this->largeurBordure ,this->getPosition().x,this->getPosition().y+this->getSizey()-this->largeurBordure); //grande bordure qui fait toute la largeur de la salle
		borders.at(3).push_back(*borderette);
	}
	//}}}
}
//}}}
//La fonction de check d'intersection avec les bordures---------------------- {{{
bool Salle::checkIntersectionBordures(Entite& entite)
{
	bool intersection=false;
	std::vector<std::vector<Border> >::iterator row;  //mieux itérateur!
	std::vector<Border>::iterator col;
	for (row=borders.begin();row!=borders.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			if (col->checkIntersection(entite))
			{
				intersection=true;
			}

		}
	}
	return intersection;
}

//}}}
//Create enemies---------------------- {{{
void Salle::createEnemies(int nombreEnemies)
{
	Arme * armette;
	armette=new Arme("Charsets/robot_charset.png",0,0,0.5,25,150,100,1,50,"WorldDestructor");
	Enemies * enemite;
	enemite=new Enemies("Charsets/robot_charset.png",4,4,0.5,25,this->getPosition().x+this->getSizex()/2,this->getPosition().y+this->getSizey()/2,50.0,10,*armette,50.0);
	this->enemies.push_back(*enemite);
}

//}}}
//Create Items---------------------- {{{
void Salle::createItems(int nombreItems)
{
	Arme * armette;
	armette=new Arme("Textures/epee1.png",0,0,0.1,300,this->getPosition().x+this->getSizex()/3,this->getPosition().y+this->getSizey()/3,3,50,"El Fabulos Epee");
	this->items.push_back(*armette);
}

//}}}
//Check enemies---------------------- {{{
Characters& Salle::checkEnemies(Characters& C)
{
	Enemies* closestEnemy=NULL;
	if (!enemies.empty())
	{
		closestEnemy=&enemies.at(0);
		for (auto & enemy : this->enemies)
		{
			if (sqrt(pow(enemy.getPosition().x-C.getPosition().x,2)+pow(enemy.getPosition().y-C.getPosition().y,2))<sqrt(pow(closestEnemy->getPosition().x-C.getPosition().x,2)+pow(closestEnemy->getPosition().y-C.getPosition().y,2)))
			{
				closestEnemy=&enemy;
			}

		}

	}
	return *closestEnemy;
}
//}}}
//Check items---------------------- {{{
Arme& Salle::checkItems(Characters& C)
{
	Arme* closestItem=NULL;
	if (!items.empty())
	{
		closestItem=&items.at(0);
		for (auto & item : this->items)
		{
			if (sqrt(pow(item.getPosition().x-C.getPosition().x,2)+pow(item.getPosition().y-C.getPosition().y,2))<sqrt(pow(closestItem->getPosition().x-C.getPosition().x,2)+pow(closestItem->getPosition().y-C.getPosition().y,2)))
			{
				closestItem=&item;
			}
//
		}
//
	}
	return *closestItem;
}
//}}}
