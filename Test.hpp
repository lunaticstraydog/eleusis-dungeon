#ifndef TEST_HPP
#define TEST_HPP
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <math.h>
#include "objets.hpp"
#include "entites.hpp"
#include "game.hpp"
#include "item.hpp"


class Test
{
public:
    Test(){};
    ~Test(){};
    void testPrendreDegat(int degat);


};


#endif
