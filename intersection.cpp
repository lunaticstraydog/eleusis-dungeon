
/*
 *
 *Author: Quentin Levent
 *
 */
#include <iostream>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/String.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include "intersection.hpp"
bool intersection(const std::vector<int> vectTop1,const std::vector<int> vectTop2)
{

	const std::vector<int>* vectTop;
	const std::vector<int>* vectBottom;
	const std::vector<int>* vectLeft;
	const std::vector<int>* vectRight;
	//std::cout << "rectangle 1 left vaut "<< vectTop1.at(1) << std::endl;
	//std::cout << "rectangle 2 left vaut "<< vectTop2.at(1) << std::endl;
	//std::cout << "rectangle 1 height vaut "<< vectTop1.at(3) << std::endl;
	if (vectTop1.at(0)<vectTop2.at(0) )
	{
		vectTop=&vectTop1;
		vectBottom=&vectTop2;
	}
	else
	{
		vectTop=&vectTop2;
		vectBottom=&vectTop1;
	}
	if (vectTop1.at(1)<vectTop2.at(1) )
	{
		vectLeft=&vectTop1;
		vectRight=&vectTop2;
	}
	else
	{
		vectLeft=&vectTop2;
		vectRight=&vectTop1;
	}

	if (vectBottom->at(0) <=(vectTop->at(0)+vectTop->at(3)) && vectRight->at(1) <= (vectLeft->at(1)+vectLeft->at(2)))
	{
		//std::cout << "rectangle 1 position y vaut "<< vectTop1.at(0) << std::endl;
		//std::cout << "rectangle 1 position x vaut "<< vectTop1.at(1) << std::endl;
		//std::cout << "rectangle 1 width vaut "<< vectTop1.at(2) << std::endl;
		//std::cout << "rectangle 1 height vaut "<< vectTop1.at(3) << std::endl;
		//std::cout << "rectangle 2 position y vaut "<< vectTop2.at(0) << std::endl;
		//std::cout << "rectangle 2 position x vaut "<< vectTop2.at(1) << std::endl;
		//std::cout << "rectangle 2 width vaut "<< vectTop2.at(2) << std::endl;
		//std::cout << "rectangle 2 height vaut "<< vectTop2.at(3) << std::endl;
		return true;
	}
	else return false;
}
