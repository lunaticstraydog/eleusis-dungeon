/*
 *
 *Author: Quentin Levent
 *
 */
#include <SFML/System/Sleep.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Window.hpp>
#include <SFML/System/String.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/String.hpp>
#include <iostream>
#include <math.h>
#include "labyrinthe.hpp"
#include "salle.hpp"

//Creation des bordures autour des salles---------------------- {{{
void Labyrinthe::createBorders()
{
	std::vector<int> sallesAdja;
	std::vector<std::vector<Salle> >::iterator row;  //mieux itérateur!
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			sallesAdja=checkAdjacent(col->getCoords());
			col->createBorders(sallesAdja);
		}
	}

}
//}}}
//Le créateur de labyrinthe TADAA---------------------- {{{
//On lui passe que des salles carrées parce que c'est déja assez compliqué comme ça
Labyrinthe::Labyrinthe(int nbrSallesLignes,sf::Window& fenetre) //idéalement on passe a laby le nombre d'ennemis et leur placement et tout
	/* Laby, dans son immense sagesse, place la première salle (en partant de en haut a gauche) avec une taille semi aléatoire (entre des bornes quoi)
		 Puis Il (loués soient ses dédales) crée  les autres salles selon y (de moins en moins aléatoirement, puis les dernières ajustées en fonction des premières.
		 Ensuite Il (que ses salles soient chantées) procéde de même pour la colonne suivante, avec moins d'aléatoire selon x, et a la septième salle, il se reposa.
		 */
{
	//init srand pour de l'aléatoire de qualitai
	std::srand(static_cast<unsigned>(time(NULL)));
	int taillex=fenetre.getSize().x;
	int tailley=fenetre.getSize().y;
	int c=2;
	float randFactor=exp(nbrSallesLignes)/(c*pow(nbrSallesLignes,sqrt(nbrSallesLignes)));
	int moyx=taillex/nbrSallesLignes; //taille moyenne d'une salle en x
	int moyy=tailley/nbrSallesLignes;
	int maxx=0;
	int maxy=0;

	for (int i=0;i<nbrSallesLignes;i++)
	{
		salles.push_back(std::vector<Salle>());
		for (int j=0;j<nbrSallesLignes;j++)
		{
			//a partir d'ici on crée une salle
			// on balance d'abord des valeurs au pif, on vérifiera plus tard si c'est chill
			int valx=(rand() % static_cast<int>(moyx*randFactor - moyx/randFactor + 1)+moyx/randFactor);
			int valy=(rand() % static_cast<int>(moyy*randFactor - moyy/randFactor + 1)+moyy/randFactor);



			std::vector<std::vector<Salle> >::iterator row;  //mieux itérateur!
			std::vector<Salle>::iterator col;

			for (row=salles.begin();row!=salles.end();++row)
			{
				for (col=row->begin();col!=row->end();++col)
				{
					if (i!=0 && row==(i-1+salles.begin()))
					{
						//std::cout << "taille col prec vaut "<<  col->getSizex()<< std::endl;
					}
					if (i!=0 && row==(i-1+salles.begin()) && col->getPosition().y+col->getSizey()>maxy) //fuites mémoires toussa FIXED (peut etre?)
					{
						maxy=col->getPosition().y+col->getSizey(); //on récupère la val max en x de la colonne précédente sauf si on est a la 1ere
						//cette valeur sera le point de départ en x de toutes les colonnes suivantes
					}

				}
			}

			if (j!=0)
			{
				maxx=salles.at(i).at(j-1).getSizex()+salles.at(i).at(j-1).getPosition().x;
			}
			if (j==nbrSallesLignes-1) //genre on est en a droite, donc on remplit hophophop
			{
				valx=taillex-maxx; //genre on est à la dernière colonne, alors on remplit avec ce qui reste
			}
			else
			{
				while ((static_cast<int>(moyx/randFactor)*(nbrSallesLignes-j-1))>(taillex-(maxx+valx))) //tant que la taille de la salle est trop grande par rapport a la place restante avec la valeur min x
				{
					valx=(rand() % static_cast<int>(moyx*randFactor - moyx/randFactor + 1)+moyx/randFactor);
				}
			}
			if (i==nbrSallesLignes-1) //-1 parceque i va de 0 a 2
			{
				valy=tailley-maxy;
			}
			else
			{
				while ((static_cast<int>(moyy/randFactor)*(nbrSallesLignes-i-1))>(tailley-(maxy+valy))) //tant que la taille de la salle est trop grande par rapport a la place restante avec la valeur min y
				{
					valy=(rand() % static_cast<int>(moyy*randFactor - moyy/randFactor + 1)+moyy/randFactor);
				}

			}
			Salle* salopette;
			//if (i!=0 && j!=0) //alors on prend comme ref le coté bas gauche de la texture précédente
			{
				salopette=new Salle("Textures/eleusis-fond.png",0,0,1.0,valx,valy,maxx,maxy,i,j); //on ajoute la hauteur pour s'aligner sur la case précédente
			}
			salles[i].push_back(*salopette);
			maxx=0;maxy=0;
		}
	}
	this->createBorders(); //crée les bordures autour des salles
};
//}}}
//La fonction d'affichage de toutes les salles---------------------- {{{
void Labyrinthe::render(sf::RenderTarget& target)
{
	std::vector<std::vector<Salle> >::iterator row;  //mieux itérateur!
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			col->render(target);
		}
	}
}
//}}}
//Fonction check adjacent qui dit ou est ce qu'il y a des salles adjacentes---------------------- {{{
//retour: left - right -up - down -> 1001 = salle adjacente a gauche et en bas
// coords i j -> coords.x=i
std::vector<int>& Labyrinthe::checkAdjacent(sf::Vector2f& coords) //FIXME tout est inversé
{
	this->sallesAdjacentes={0,0,0,0};
	if (coords.x!=0)
	{
		if (salles.at(coords.x-1).at(coords.y).checkIntersection(salles.at(coords.x).at(coords.y))) { sallesAdjacentes.at(2)=1; }
		//pas besoin de else, le vecteur est déjà rempli de 0
	}
	if (coords.x!=salles.size()-1)
	{
		if (salles.at(coords.x+1).at(coords.y).checkIntersection(salles.at(coords.x).at(coords.y))) { sallesAdjacentes.at(3)=1;}
	}
	if (coords.y!=0)
	{
		if (salles.at(coords.x).at(coords.y-1).checkIntersection(salles.at(coords.x).at(coords.y))) { sallesAdjacentes.at(0)=1;}
	}
	if (coords.y!=salles.at(coords.y).size()-1) //bon normalement on pourrait juste utiliser le critère au dessus
	{
		if (salles.at(coords.x).at(coords.y+1).checkIntersection(salles.at(coords.x).at(coords.y))) { sallesAdjacentes.at(1)=1;}
	}
	for (int i=0;i<4;i++)
	{
		//std::cout << " col 1 vaut "<< sallesAdjacentes.at(i) << std::endl;
	}
	return this->sallesAdjacentes;
}
//}}}
//Fonction d'update standard de tout ce qui est dans le laby---------------------- {{{
bool Labyrinthe::update(sf::Window& fenetre)
{
	this->readyForNextTurn=true;
	std::vector<std::vector<Salle> >::iterator row;  //On itère tranquillou
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			if (!col->update(fenetre,*this))
			{
				this->readyForNextTurn=false; //il y au moins une salle dans laquelle un ennemi est pas prêt
			}
		}
	}
	return readyForNextTurn;
}

//}}}
//Fonction d'update lorsqu'on passe au tour suivant ---------------------- {{{
void Labyrinthe::tourSuivant(Player& player)
{
	std::vector<std::vector<Salle> >::iterator row;  //On itère tranquillou
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			sf::sleep(sf::milliseconds(10));
			col->tourSuivant(player);
		}
	}

}

//}}}
//La fonction de check d'intersection avec les bordures---------------------- {{{
bool Labyrinthe::checkIntersectionBordures(Entite& entite)
{
	bool intersection=false;
	std::vector<std::vector<Salle> >::iterator row;  //mieux itérateur!
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			if (col->checkIntersectionBordures(entite))
			{
				intersection=true;
			}
		}
	}
	return intersection;
}

//}}}
//Initialisation du laby---------------------- {{{
void Labyrinthe::initLaby()
{
	std::vector<std::vector<Salle> >::iterator row;  //mieux itérateur!
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			col->createEnemies(1);
			col->createItems(1);
		}
	}

}
//}}}
//Fonction qui check si il y a des ennemis a proximité pour attaquer---------------------- {{{
Characters& Labyrinthe::checkEnemies(Characters& C)
{
	Salle * salleLaPlusProche;
	std::vector<std::vector<Salle> >::iterator row;  //mieux itérateur!
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			if (col->checkIntersection(C))
				 {
					salleLaPlusProche=&*col;
				 }
		}
	}
	Characters& closestEnemy=salleLaPlusProche->checkEnemies(C);
	return closestEnemy;
}
//}}}
//Fonction qui check si il y a des items a proximité pour attaquer---------------------- {{{
Arme& Labyrinthe::checkItems(Characters& C)
{
	Salle * salleLaPlusProche;
	std::vector<std::vector<Salle> >::iterator row;  //mieux itérateur!
	std::vector<Salle>::iterator col;
	for (row=salles.begin();row!=salles.end();++row)
	{
		for (col=row->begin();col!=row->end();++col)
		{
			if (col->checkIntersection(C))
				 {
					salleLaPlusProche=&*col;
				 }
		}
	}
	Arme& closestItem=salleLaPlusProche->checkItems(C);
	return closestItem;
}
//}}}
