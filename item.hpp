#ifndef ITEM_HPP
#define ITEM_HPP
#include <SFML/System/String.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <math.h>
#include "objets.hpp"
//#include "entites.hpp"
class Characters;
//La classe item qui dérive d'objet---------------------- {{{
class  Item : public Objet
{
	//Attributs et methodes d'init---------------------- {{{
	protected:
		sf::String type;
		sf::String nom;
		bool ramassable=true;

		//}}}
	public :
		//Constructeur---------------------- {{{
		Item(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int posX,int posY,sf::String nom): Objet(cheminText,ligne,colonne,scale,spriteSizex,posX,posY) {this->nom=nom;}
		void etreOuNePasEtreRamasse(bool ramasseOuNon)
		{
			ramassable=ramasseOuNon;
			 this->aAfficher=ramasseOuNon;

		}
		bool isRamassable()
		{
			return this->ramassable;
		}
		sf::String getNom()
		{
			return this->nom;
		}
		virtual void use(Characters& perso){};
		virtual ~Item(){};
};
//}}}
//}}}
//La classe Arme qui dérive d'Item---------------------- {{{
class  Arme : public Item
{
	//Attributs et methodes d'init---------------------- {{{
	protected:
		char type='A';
		int portee;
		int nombreDegat;

		//}}}
	public :
		//Constructeur---------------------- {{{
		Arme(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int posX,int posY,int nombreDegat,int portee,sf::String nom): Item(cheminText,ligne,colonne,scale,spriteSizex,posX,posY,nom) {this->nombreDegat=nombreDegat; this->portee=portee;this->nom=nom;}
		void use(Characters& perso);
		int getDegats()
		{
			return this->nombreDegat;
		}
		char getType()
		{
			return this->type;
		}
};
//}}}
//}}}
#endif
