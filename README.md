Eleusis Dungeon

![image](screenshot.png)
Bibliothèques :
- SFML

Instructions pour compiler :

Lancer la commande "Make"
Puis "./Main" pour lancer le jeu

Manuel d'utilisation :

Le but du jeu est de trouver la sortie du labyrinthe tout en évitant les ennemies et en ramassant des objets.
Le jeu est basé sur un système de tour par tour.
Dans ce jeu, vous êtes dans la peau de ROB, un robot humanoïde accompagné par son fidèle acolyte Roby.
Il y a donc deux joueurs, qui jouent l'un après l'autre.
Toute entrée du joueur (clic pour se déplacer, entrée pour attaquer, r pour récupérer un objet) compte comme une action.
Lorsque le premier a joué, le second joue, puis c'est au tour des ennemis qui se rueront sur vous pour vous faire goûter de leur lame (dans la limite de leur portée)
Pas mal d'infos seront affichées dans le terminal (HP, dégats faits...)
Le joueur se déplace en cliquant sur l'endroit où il veut se déplacer sur la map.
Le joueur doit passer d'une salle à l'autre en traversant les portes, représentées par une absence de murs. S'il se cogne contre un mur, il va simplement rebondir dessus.
Le joueur attaque les ennemis avec la touche " entrée ", uniquement si il a une arme en main, sinon cela échoue.
Le joueur utilise la touche " r " pour récupérer des objets en passant proche d'eux.
Les ennemis ont la même texture que les joueurs car ce sont vos doubles maléfiques, et vous êtes vous même le jumeau de Roby. (Ok on avait pas assez de textures)
Attention, une fois que vous avez tué un ennemi, ce dernier reste en vie sous forme de fantôme et continue de vous attaquer. (ne restez pas vers le lieu de votre combat.)
La caméra est centré sur un seul des deux joueurs.


Le jeu se termine quand un des 2 joueurs meurt ou bien quand l'un des 2 joueurs arrive à la sortie qui se trouve dans la dernière salle en bas à droite du donjon en théorie.
Bonne chance dans votre quête !



On notera que le nombre d'ennemis(un par salle), leurs armes et propriétés (toutes les mêmes) sont peu recherchées mais leur raison d'être était de montrer ce qu'il était possible de faire.
Par construction, il serait aisé de créer des ennemis avec des sprites et armes différents.
De même pour les items: il n'y en a ici qu'un seul type (une épée) mais il serait facile d'en créer d'autres en gardant une structure similaire.

Les collisions entre le joueur et les bordures peuvent poser problème dans certaines configurations très particulières. (on a le joueur qui peut rebondir entre 2 bordures s'il est dans un coin dans certains cas.)
Nous n'avons pas fait beaucoup de tests unitaires, car la plupart de nos fonctions servent à faire de l'affichage.
