
/*
*
*Author: Quentin Levent
*
*/
#ifndef GAME_HPP
#define GAME_HPP
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/ThreadLocalPtr.hpp>
#include <iostream>
#include "entites.hpp"
#include "labyrinthe.hpp"
//Classe du jeu---------------------- {{{
class Game
{
	//Attributs et fonctions protected---------------------- {{{
protected:
	//Window
	sf::RenderWindow* window;
	sf::View view; //la vue centrée sur le joueur
	//GUI
	sf::Font font;
	bool readyForNextTurn; //s'occupe de gérer les actions du jeu
	bool passageTourSuivant; //déclenche le passage au... bah... tour suivant. Vous savez lire?
	sf::RectangleShape playerHpBar;
	sf::RectangleShape playerHpBarBackground;

	//Player
	Player* player;
	Player* player2;
	Labyrinthe* laby;

	//Private functions
	void initWindow();
	void updateView();
	void initPlayer();
	void tourSuivant();

	//}}}
	//fonctions publiques---------------------- {{{
public:
	Game();
	virtual ~Game();

	//Functions
	void run();

	void updatePollEvents();
	void updateInput();
	bool update();

	void render();
	void initGui();
	void renderGui();
};
//}}}
//}}}
#endif
