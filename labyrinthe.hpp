#include <SFML/System/String.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include "salle.hpp"
#include "entites.hpp"
#include "intersection.hpp"
#ifndef LABYRINTHE_HPP
#define LABYRINTHE_HPP

//Un labyrinthe,qui constitue un niveau ---------------------- {{{
class Labyrinthe
{
	protected:
		std::vector<std::vector<Salle>> salles; //un vecteur de salles en x et y -> salles[i][j] = ligne i col j
		std::vector<int> sallesAdjacentes{0,0,0,0}; //variable renvoyée lors du cgeck si la salle est adjacente
		bool readyForNextTurn=true; //Dit si el labyrintho est prêt a passer au prochain tour

	public :
		void render(sf::RenderTarget& target) ;
		Labyrinthe(int nbrSallesLignes,sf::Window& fenetre);
		std::vector<int>& checkAdjacent(sf::Vector2f& coords);
		void createBorders();
		bool update(sf::Window& fenetre);
		void tourSuivant(Player& player);
		bool checkIntersectionBordures(Entite& entite);
		void initLaby();
		Characters& checkEnemies(Characters& C);
		Arme& checkItems(Characters& C);
		//On utilise la fonction intersect codée pour l'occasion, car celle de sfml n'est pas inclusive sur les bords des rectangles. Et c'est toujours relou les trucs pas inclusif·ves
};
//}}}
#endif
