/*
 *
 *Author: Quentin Levent
 *
 */
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <math.h>
#include <string>
#include "entites.hpp"
#include "item.hpp"
#include "labyrinthe.hpp"

//Déf des destructeurs parce que sinon boum---------------------- {{{
Entite::~Entite() {};
Player::~Player() {};
//}}}
//Fonctions communes aux humanoides---------------------- {{{
void Characters::useItem(Characters& C)
{
	if (this->curItem!=NULL)
	{
				this->curItem->use(C);
	}
	else std::cout << "Vous n'avez rien en main, echec critique, vous tombez en faisant des moulinets" << std::endl;
}
 void Characters::PrendreDegat(int nombreDegats)

{
	std::cout << "L'ennemi prend "<< nombreDegats << " degats"<< std::endl;
	this->health-=nombreDegats;
}
void Characters::ramasser(Arme& toRamasser)
{
	this->curItem=&toRamasser;
	if (listeItem.at(9)!=NULL)
	{
		std::cout << "Inventaire Plein !" << std::endl;
	}
	else
	{
		int i=0;
		while (listeItem.at(i)!=NULL)
		{
			i++;
		}
		listeItem.at(i)=&toRamasser;
		this->curItem=listeItem.at(i);
		toRamasser.etreOuNePasEtreRamasse(false);
	}
}
//Update - Fonctions communes a toutes ce qui derive de character---------------------- {{{
bool Characters::update(sf::Window& fenetre,Labyrinthe& laby)
{
	float thresh=1; //la limite a laquelle on s'arrête
	if (fabs(this->ObjectifCourant.x-this->getPosition().x)>thresh || fabs(this->ObjectifCourant.y-this->getPosition().y)>thresh) //si x ou y !=position courante
	{
		if ((fabs(this->ObjectifCourant.x-this->getPosition().x) < thresh && fabs(this->ObjectifCourant.y-this->getPosition().y) < thresh) || (laby.checkIntersectionBordures(*this)))
		{
			if (laby.checkIntersectionBordures(*this))
			{
				float tempPositionX=this->ObjectifCourant.x;
				float tempPositionY=this->ObjectifCourant.y;
				this->ObjectifCourant.x=this->getPosition().x+1*(this->getPosition().x-tempPositionX);
				this->ObjectifCourant.y=this->getPosition().y+1*(this->getPosition().y-tempPositionY);
				smoothmove(this->ObjectifCourant,fenetre);
			}
			else if ((fabs(this->ObjectifCourant.x-this->getPosition().x) < thresh && fabs(this->ObjectifCourant.y-this->getPosition().y) < thresh))
			{
				this->ObjectifCourant.x=this->getPosition().x;this->ObjectifCourant.y=this->getPosition().y;
			}
		}
		else
		{
			smoothmove(this->ObjectifCourant,fenetre);
			this->readyForNextTurn=false; //tant qu'on bouge, on est pas prêt pour le tour suivant.
		}
	}
	else this->readyForNextTurn=true; //on a atteint la destination
	if (this->getHp()<=0)
		 {
			 this->aAfficher=false;
		 }
	return readyForNextTurn;
}
//}}}
//}}}
//Update - Fonctions communes a toutes les entités---------------------- {{{
bool Entite::update(sf::Window& fenetre,Labyrinthe& laby)
{
	float thresh=1; //la limite a laquelle on s'arrête
	if (fabs(this->ObjectifCourant.x-this->getPosition().x)>thresh || fabs(this->ObjectifCourant.y-this->getPosition().y)>thresh) //si x ou y !=position courante
	{
		if ((fabs(this->ObjectifCourant.x-this->getPosition().x) < thresh && fabs(this->ObjectifCourant.y-this->getPosition().y) < thresh) || (laby.checkIntersectionBordures(*this)))
		{
			if (laby.checkIntersectionBordures(*this))
			{
				float tempPositionX=this->ObjectifCourant.x;
				float tempPositionY=this->ObjectifCourant.y;
				this->ObjectifCourant.x=this->getPosition().x+1*(this->getPosition().x-tempPositionX);
				this->ObjectifCourant.y=this->getPosition().y+1*(this->getPosition().y-tempPositionY);
				smoothmove(this->ObjectifCourant,fenetre);
			}
			else if ((fabs(this->ObjectifCourant.x-this->getPosition().x) < thresh && fabs(this->ObjectifCourant.y-this->getPosition().y) < thresh))
			{
				this->ObjectifCourant.x=this->getPosition().x;this->ObjectifCourant.y=this->getPosition().y;
			}
		}
		else
		{
			smoothmove(this->ObjectifCourant,fenetre);
			this->readyForNextTurn=false; //tant qu'on bouge, on est pas prêt pour le tour suivant.
		}
	}
	else this->readyForNextTurn=true; //on a atteint la destination
	return readyForNextTurn;
}
//}}}
//Fonction des ennemis---------------------- {{{
void Enemies::AutoAttack(Player& Joueur)
{
	if (abs(abs(this->getPosition().x-Joueur.getPosition().x)+abs(this->getPosition().y-Joueur.getPosition().y))<this->seuilMove) { this->setObjectif(Joueur.getPosition()); }

	if (abs(abs(this->getPosition().x-Joueur.getPosition().x)+abs(this->getPosition().y-Joueur.getPosition().y))<this->seuilAttack)
	{ this->useItem(Joueur); }
}
void Enemies::initEnemies(Arme& arme)
{
	this->curItem=&arme;
}
//}}}
//fonctions spé au joueur---------------------- {{{
void Player::PrendreDegat(int nombreDegats)
{
	std::cout << "Vous prenez "<< nombreDegats << " degats"<< std::endl;
	this->health-=nombreDegats;
}
//}}}
//Mouvement fluide et directionnel---------------------- {{{

void Entite::smoothmove(sf::Vector2f& objectif,sf::Window& fenetre)
{
	float dirX=-this->getPosition().x/fenetre.getSize().x+objectif.x/fenetre.getSize().x;
	float dirY=-this->getPosition().y/fenetre.getSize().y+objectif.y/fenetre.getSize().y;
	this->move(dirX,dirY);
	bool vert=fabs(objectif.y-this->getPosition().y)>fabs(objectif.x-this->getPosition().x); //indique si l'obj est plutôt vertical ou horizontal
	if (vert && (this->ObjectifCourant.y<this->getPosition().y)) //l'objectif en haut
	{
		this->sprite.setTextureRect(sf::IntRect(curframe*this->spriteSizex,3*this->spriteSizex,this->spriteSizex,this->spriteSizex)); //on load la texture haute
	}
	if (vert && (this->ObjectifCourant.y>this->getPosition().y)) //l'objectif en bas
	{
		this->sprite.setTextureRect(sf::IntRect(curframe*this->spriteSizex,0*this->spriteSizex,this->spriteSizex,this->spriteSizex)); //on load la texture vers bas
	}
	if (!vert && (this->ObjectifCourant.x>this->getPosition().x)) //l'objectif est a droite
	{
		this->sprite.setTextureRect(sf::IntRect(curframe*this->spriteSizex,2*this->spriteSizex,this->spriteSizex,this->spriteSizex)); //on load la texture droite
	}
	if (!vert && (this->ObjectifCourant.x<this->getPosition().x)) //l'objectif est a gauche
	{
		this->sprite.setTextureRect(sf::IntRect(curframe*this->spriteSizex,1*this->spriteSizex,this->spriteSizex,this->spriteSizex)); //on load la texture gauche
	}
	if (curframe==2 && compteframes==10) {curframe=0;compteframes=0;}
	else if (compteframes==10) {curframe++; compteframes=0;}
	else compteframes++;
}
//fonction d'update pour assurer le mouvement
bool update(sf::Window& fenetre);
//}}}
