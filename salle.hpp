#include <SFML/System/String.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>
//#include <bits/types/cookie_io_functions_t.h>
#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include "objets.hpp"
#include "entites.hpp"
#include "item.hpp"
#ifndef SALLE_HPP
#define SALLE_HPP
//class Item;
//Une salle unique,dont un niveau est constitué---------------------- {{{
class Border : public Objet
{
	public:
		Border(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex, int spriteSizey,int posX,int posY): Objet(cheminText,ligne,colonne,scale,spriteSizex,spriteSizey,posX,posY) {};
};

class Salle : public Objet
{
	protected:
		std::vector<Enemies> enemies;
		std::vector<Arme> items;
		std::vector<std::vector<Border>> borders; //un vecteur de bordures avec leurs coordonnées et tout
		sf::Vector2f coords;
		int tailleDemiPorte=30; //taille de la MOITIE de la porte de chaque salle, en pixels
		int largeurBordure=10; //taille de la bordure de chaque salle, en pixels
		sf::String textureBordure="Textures/eleusis-bordure.png";
		bool readyForNextTurn=true; //Dit si el salopette est prête

	public :
		Salle(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex, int spriteSizey,int posX,int posY,int coordx,int coordy): Objet(cheminText,ligne,colonne,scale,spriteSizex,spriteSizey,posX,posY) {coords.x=coordx; coords.y=coordy;};
		void render(sf::RenderTarget& target) ;
		void createBorders(std::vector<int>& adjacentRooms);
		void tourSuivant(Player& player);
		bool update(sf::Window& fenetre,Labyrinthe& laby);
		bool checkIntersectionBordures(Entite& entite);
		void createEnemies(int nombreEnemies);
		void createItems(int nombreEnemies);
		Characters& checkEnemies(Characters& C);
		Arme& checkItems(Characters& C);
		sf::Vector2f& getCoords(){return this->coords;}
};
//}}}
#endif
