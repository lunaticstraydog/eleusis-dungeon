/*
 *
 *Author: Quentin Levent
 *
 */
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <math.h>
#include "objets.hpp"
void Objet::render(sf::RenderTarget& target)
{
	this->vectTop.at(0)=this->getPosition().y;
	this->vectTop.at(1)=this->getPosition().x;
	this->vectTop.at(2)=this->getSizex();
	this->vectTop.at(3)=this->getSizey();
	if (this->aAfficher)
	{
		target.draw(this->sprite);
	}
}

//ligne ->numéro de la ligne au se trouve le début des sprite du perso
//colonne ->numéro de la colonne au se trouve le début des sprite du perso
Objet::Objet(sf::String cheminText,int ligne,int colonne,float scale,int spriteSize,int posX,int posY) //pour les sprite carrés
{
	this->spriteSizex=spriteSize,this->spriteSizey=spriteSize;
	if (!this->texture.loadFromFile(cheminText))
	{
		std::cout << "Erreur: Pas pu charger la texture" << "\n";
	}
	this->sprite.setTexture(this->texture);
	this->sprite.scale(scale,scale);
	this->sprite.setTextureRect(sf::IntRect(ligne*this->spriteSizex,colonne*this->spriteSizex,this->spriteSizex,this->spriteSizex)); //sprite vu de face
	sprite.setOrigin(sf::Vector2f((float)this->spriteSizex/2,(float)this->spriteSizex/2));
	this->setPosition(posX,posY);
	this->vectTop.push_back(this->getPosition().y);
	this->vectTop.push_back(this->getPosition().x);
	this->vectTop.push_back(this->getSizex());
	this->vectTop.push_back(this->getSizey());

}
Objet::Objet(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int spriteSizey,int posX,int posY) //pour les sprites rectangulaires
{
	this->spriteSizex=spriteSizex;
	this->spriteSizey=spriteSizey;
	if (!this->texture.loadFromFile(cheminText))
	{
		std::cout << "Erreur: Pas pu charger la texture" << "\n";
	}
	this->sprite.setTexture(this->texture);
	this->sprite.scale(scale,scale);
	this->sprite.setTextureRect(sf::IntRect(ligne*this->spriteSizex,colonne*this->spriteSizey,this->spriteSizex,this->spriteSizey)); //sprite vu de face
	//sprite.setOrigin(sf::Vector2f((float)this->spriteSizex/2,(float)this->spriteSizex/2)); //il vaut mieux placer depuis en haut a gauche dans ce cask
	this->setPosition(posX,posY);
	this->vectTop.push_back(this->getPosition().y);
	this->vectTop.push_back(this->getPosition().x);
	this->vectTop.push_back(this->getSizex());
	this->vectTop.push_back(this->getSizey());
}
