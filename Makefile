Main: game.o entites.o salle.o labyrinthe.o objets.o intersection.o item.o Test.o
	g++ -o Main game.o entites.o salle.o labyrinthe.o objets.o intersection.o item.o Test.o -lsfml-graphics -lsfml-window -lsfml-system 
game.o:game.cpp game.hpp
	g++ -g -c game.cpp
entites.o:entites.cpp entites.hpp
	g++ -g -c entites.cpp
salle.o:salle.cpp salle.hpp
	g++ -g -c salle.cpp
labyrinthe.o:labyrinthe.cpp labyrinthe.hpp
	g++ -g -c labyrinthe.cpp
objets.o:objets.cpp objets.hpp
	g++ -g -c objets.cpp
intersection.o:intersection.cpp intersection.hpp
	g++ -g -c intersection.cpp
item.o:item.cpp item.hpp
	g++ -g -c item.cpp

Test.o: Test.cpp Test.hpp
	g++ -g -c Test.cpp
clean :
	rm -f *.o Main
