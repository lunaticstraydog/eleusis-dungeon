
/*
 *
 *Author: Quentin Levent
 *
 */
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Sleep.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <iostream>
#include "game.hpp"
#include <math.h>
#include "entites.hpp"
//Game windows functions---------------------- {{{
void Game::initWindow()
{
	this->window = new sf::RenderWindow(sf::VideoMode(1200,675), "Eleusis Dungeon", sf::Style::Close | sf::Style::Titlebar);
	this->window->setFramerateLimit(60);
	this->window->setVerticalSyncEnabled(false);
	sf::sleep(sf::milliseconds(100)); //sinon ya des trucs pas initialisés
}

void Game::updateView()
{
	this->view.setCenter(this->player->getPosition());
	this->view.setSize(sf::Vector2f(600.f, 400.f));
}


bool Game::update()
{
	this->updateInput();
	this->readyForNextTurn=true;
	if (!this->player->update(*this->window,*this->laby))
	{
		this->readyForNextTurn=false;
	}

	if (!this->player2->update(*this->window,*this->laby))
	{
		this->readyForNextTurn=false;
	}

	if (!this->laby->update(*this->window) )
	{
		this->readyForNextTurn=false;
	}
	if (this->passageTourSuivant && this->player->isReadyForNextTurn())
	{
		this->tourSuivant();
	}
	return this->readyForNextTurn; //bon en vrai on s'en bat un peu les reins de ça, mais ça pourrait servir
}


void Game::renderGui()
{
	this->window->draw(this->playerHpBarBackground);
	this->window->draw(this->playerHpBar);
}



void Game::render()
{
	this->updateView();
	this->window->clear();
	this->window->setView(view);
	//Draw all the stuffs
	this->laby->render(*this->window);
	this->player->render(*this->window);
	this->player2->render(*this->window);
	this->renderGui();
	this->window->display();
}
//}}}
//Game initialisation functions---------------------- {{{
void Game::initPlayer()
{
	this->player = new Player("Charsets/robot_charset.png",0,0,0.5,25,150,100,50.0,20,200.0);
	this->player2 = new Player("Charsets/robot_charset.png",4,0,0.5,25,100,120,50.0,20,200.0);
	this->laby = new Labyrinthe(3,*this->window) ;
	this->laby->initLaby();
}

//Con/Des
Game::Game()
{
	this->initWindow();
	this->initPlayer();
	this->updateView();
}

Game::~Game()
{
	delete this->window;
	delete this->player;
}

//Functions
void Game::run()
{
	while (this->window->isOpen())
	{
		this->updatePollEvents();
		this->update();
		this->render();
	}
}
void Game::initGui()
{
	this->playerHpBar.setSize(sf::Vector2f(300.f,25.f));
	this->playerHpBar.setFillColor(sf::Color::Red);
	sf::Vector2f pixelPos;
	pixelPos=sf::Vector2f(20.f,20.f);
	sf::Vector2i localPos=this->window->mapCoordsToPixel(pixelPos);
	this->playerHpBar.setPosition((sf::Vector2f)localPos);
	this->playerHpBarBackground=this->playerHpBar;
	this->playerHpBarBackground.setFillColor(sf::Color(25,25,25,200));
}
//}}}
//Game update functions---------------------- {{{
void Game::updatePollEvents()
{
	sf::Event e;
	while (this->window->pollEvent(e))
	{
		if (e.Event::type == sf::Event::Closed)
		{
			this->window->close();
		}
		if (e.Event::KeyPressed && e.Event::key.code == sf::Keyboard::Escape)
		{
			this->window->close(); //A REMETTRE
		}
	}
}

void Game::updateInput()
{
	//Move player
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		this->player->move(-1.f, 0.f);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		this->player->move(1.f, 0.f);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		this->player->move(0.f, -1.f);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		this->player->move(0.f, 1.f);
	//Keycode pour le mouvement ---------------------- {{{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && this->readyForNextTurn)
	{
		sf::Vector2i pixelPos = sf::Mouse::getPosition(*window);

		// convert it to world coordinates
		sf::Vector2f NouveauBjectif= window->mapPixelToCoords(pixelPos); //oui on perd un peu de memoire. Non c'est pas grave.
		if (!this->player->getHasPlayed())
		{
			this->player->setObjectif(NouveauBjectif);
			this->player->getHasPlayed()=true;
		}
		else
		{
			this->player2->setObjectif(NouveauBjectif);
			this->passageTourSuivant=true;
			this->player->getHasPlayed()=false;
			this->player2->getHasPlayed()=false;
		}
	}
	//}}}
		//Keycode pour l'utlisation d'objets---------------------- {{{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
		{
		std::cout << "le joueur 1 possède "<< player->getHp() << std::endl;
		std::cout << "le joueur 2 possède "<< player2->getHp() << std::endl;
		if (player->getHp()<=0 || player2->getHp()<=0)
			 {
				 std::cout << "Un de vos joueurs est mort, vous avez perdu..." << std::endl;
				 this->window->close();
			 }
		if ((player->getPosition().x>this->window->getSize().x-200 && player->getPosition().y>this->window->getSize().y-200) || (player2->getPosition().x>this->window->getSize().x-200 && player2->getPosition().y>this->window->getSize().y-200))
			 {
				 std::cout << "Vous avez atteint la fin du labyrinthe, bravo a vous!" << std::endl;
				 this->window->close();
			 }
		if (!this->player->getHasPlayed())
			 {
				 Characters* closestEnemy=&this->laby->checkEnemies(*player);
				 if (closestEnemy!=NULL)
					  {
						 this->player->useItem(this->laby->checkEnemies(*player));
						}
				 else std::cout << "Echec de l'attaque, vous avez gaché un tour" << std::endl;
				this->player->getHasPlayed()=true;
			 }
		else
		{
				 Characters* closestEnemy=&this->laby->checkEnemies(*player);
				 if (closestEnemy!=NULL)
					  {
						 this->player2->useItem(this->laby->checkEnemies(*player));
						}
				this->passageTourSuivant=true;
				this->player->getHasPlayed()=false;
				this->player2->getHasPlayed()=false;
		}
		}
		//}}}
		//Keycode pour récupérer les objets---------------------- {{{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
		if (!this->player->getHasPlayed())
			 {
				 Arme* closestItem=&this->laby->checkItems(*player);
				 if (closestItem!=NULL && closestItem->isRamassable())
					  {
						 this->player->ramasser(*closestItem);
						 std::cout << "Vous ramassez "<< (std::string)closestItem->getNom() << std::endl;
						}
				 else std::cout << "Echec du ramassage, vous avez gaché un tour" << std::endl;
				this->player->getHasPlayed()=true;
			 }
		else
		{
				 Arme* closestItem=&this->laby->checkItems(*player2);
				 if (closestItem!=NULL && closestItem->isRamassable())
					  {
						 this->player2->ramasser(*closestItem);
						 std::cout << "Vous ramassez "<< (std::string)closestItem->getNom() << std::endl;
						}
				 else std::cout << "Echec du ramassage, vous avez gaché un tour" << std::endl;
				this->passageTourSuivant=true;
				this->player->getHasPlayed()=false;
				this->player2->getHasPlayed()=false;
		}
		}
		//}}}
}
void Game::tourSuivant()
{
	laby->tourSuivant(*this->player);
	this->passageTourSuivant=false;
}
//}}}
//Main---------------------- {{{
int main()
{
	//Init srand
	std::srand(static_cast<unsigned>(time(NULL)));

	//Init Game engine
	Game game;

	//Game loop
	game.run();
	//End of application
	return 0;
}
//}}}
