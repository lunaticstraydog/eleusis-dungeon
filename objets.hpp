#ifndef OBJETS_HPP
#define OBJETS_HPP
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <istream>
#include <vector>
#include <ctime>
#include <sstream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include "intersection.hpp"
//Classe d'objet standard, qui bouge pas---------------------- {{{
class  Objet
{
	protected:
		sf::Sprite sprite;
		sf::Texture texture;
		void initTexture();
		void initSprite();
		void initVariables();
		bool aAfficher=true;
		int spriteSizex;
		int spriteSizey;
		std::vector<int> vectTop; //contient les coordonnées top left width heigth de l'objet
	public:
		Objet(sf::String cheminText,int ligne,int colonne,float scale,int spriteSize,int posX,int posY);
		Objet(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int spriteSizey,int posX,int posY);
		const  sf::Vector2f& getPosition() const { return this->sprite.getPosition(); }
		const  float getSizex() const { return this->sprite.getGlobalBounds().width;}
		const  float getSizey() const { return this->sprite.getGlobalBounds().height;}
		void render(sf::RenderTarget& target) ;
		void setPosition(const float x, const float y) { this->sprite.setPosition(x, y); }
		bool checkIntersection(Objet& rect1) //un poil hacky ce truc la
		{
			//return intersection(this->sprite.getTextureRect(),rect1.sprite.getTextureRect());
			return intersection(this->vectTop,rect1.vectTop);
		}
		virtual ~Objet(){};

};
//}}}
#endif
