#ifndef ENTITES_HPP
#define ENTITES_HPP
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Window.hpp>
#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <math.h>
#include "objets.hpp"

class Item;
class Arme;
class Labyrinthe;

//function to get the sign of a number
//template <typename T> int sgn(T val) { return (T(0) < val) - (val < T(0)); }
//La classe entite dont ce qui peut bouger dérive---------------------- {{{
class  Entite : public Objet
{
	//Attributs et methodes d'init---------------------- {{{
	protected:
		int curframe=0; //gère le numero de la frame ou on en est
		int compteframes=0; //permet de pas aller trop vite sur les frames
		float movementSpeed;
		float portee; //definit la distance max parcourue en un tour
		bool readyForNextTurn=true; //Dit si el personnagis sont prêts
		sf::Vector2f ObjectifCourant;
		//}}}
	public :
		//getters et mouvements classiques---------------------- {{{
		//Construit un objet avec les paramètres en arguments, et lui rajoute une vitesse pour pouvoir se déplacer
		Entite(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int posX,int posY,float movementSpeed,float portee): Objet(cheminText,ligne,colonne,scale,spriteSizex,posX,posY) { this->movementSpeed=movementSpeed;this->portee=portee;this->ObjectifCourant.x=this->getPosition().x;this->ObjectifCourant.y=this->getPosition().y;}

		void setObjectif(sf::Vector2f NouveauBjectif) //permet de lui donner un objectif depuis la gestion du jeu
		{
			if (abs(this->getPosition().x-NouveauBjectif.x)>this->portee) //on notera que la portee ne note que la distance max selon x ou Y a parcourir !! pas la norme !! C'est volontaire dans le sens ou sinon j'aurais jamais dormi et en plus il est déja tard alors bon hein
			{
				if (this->getPosition().x-NouveauBjectif.x<0) // l'objectif est a droite
				{
					this->ObjectifCourant.x=this->getPosition().x+portee;
				}
				else  //obejctif a gauche donc il va a la distance max a gauche
				{
					this->ObjectifCourant.x=this->getPosition().x-portee;
				}
			}
			else
			{
				this->ObjectifCourant.x=NouveauBjectif.x;
			}
			if (abs(this->getPosition().y-NouveauBjectif.y)>this->portee)
			{
				if (this->getPosition().y-NouveauBjectif.y<0) // l'objectif est en bas
				{
					this->ObjectifCourant.y=this->getPosition().y+portee;
				}
				else  //obejctif en haut donc il va a la distance max en haut
				{
					this->ObjectifCourant.y=this->getPosition().y-portee;
				}
			}
			else
			{
				this->ObjectifCourant.y=NouveauBjectif.y;
			}
		}

		void move(const float dirX, const float dirY)
		{
			this->sprite.move(this->movementSpeed * dirX, this->movementSpeed * dirY);
		}
		bool isReadyForNextTurn()
		{
			return this->readyForNextTurn;
		}

		virtual ~Entite()=0;
		//}}}
		//Mouvement fluide et directionnel---------------------- {{{

		void smoothmove(sf::Vector2f& objectif,sf::Window& fenetre);
		//fonction d'update pour assurer le mouvement
		virtual bool update(sf::Window& fenetre,Labyrinthe& laby);
		//}}}
};
//}}}
//Classe characters pour les persos humanoides---------------------- {{{
class Characters: public Entite
{
	protected :
		Characters();
		int health;
		int maxHealth;
		int capaciteInventaire=10;
		std::vector<Arme *>listeItem=std::vector<Arme *>(capaciteInventaire,NULL);
		Arme *curItem=NULL;
	public :
		//Construit une entité (qui construit un objet avec une vitesse) et lui rajoute de la vie
		Characters(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int posX,int posY,float movementSpeed,int health,float portee): Entite(cheminText,ligne,colonne,scale,spriteSizex,posX,posY,movementSpeed,portee) {this->health=health,this->maxHealth=health;}
		bool update(sf::Window& fenetre,Labyrinthe& laby);
		void useItem(Characters& P);
		virtual void PrendreDegat(int nombreDegatsconst);
		//Ramassage d'objet---------------------- {{{
		void ramasser(Arme& toRamasser);
		//}}}
		//Getters---------------------- {{{
		const int& getHp() const
		{
			return this->health;
		}
		const int& getHpMax() const
		{
			return this->maxHealth;
		}
		//}}}

};
//}}}
//Classe représentant le joueur---------------------- {{{
class Player: public Characters
{
	protected:
		bool hasPlayed=false;
	public :
		//Construit un character
		Player(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int posX,int posY, float movementSpeed,int health,float portee): Characters(cheminText,ligne,colonne,scale,spriteSizex,posX,posY,movementSpeed,health,portee) {}
		bool& getHasPlayed()
		{
			return hasPlayed;
		}
		void PrendreDegat(int nombreDegats);
		~Player();

};
//}}}
//Classe représentant les ennemis---------------------- {{{
class Enemies: public Characters
{
	protected:
		int seuilAttack=70;
		int seuilMove=200;

	public :
		//Construit un character
		Enemies(sf::String cheminText,int ligne,int colonne,float scale,int spriteSizex,int posX,int posY, float movementSpeed,int health,Arme& arme,float portee): Characters(cheminText,ligne,colonne,scale,spriteSizex,posX,posY,movementSpeed,health,portee) {initEnemies(arme);}
		void AutoAttack(Player& Joueur);
		void initEnemies(Arme& arme);
		~Enemies(){};

};
//}}}
#endif
